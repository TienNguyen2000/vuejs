var app = new Vue({
    el: '#app',
    data: {
        category: {
            id: 1,
            name: "Điện thoại"
        },
        message: 'hello World',
        isInputDisabled: false
    },
    methods: {
        handChangeTitle() {
            this.message = this.message + ' 123456';
        },
        handleChangeStatusInput() {
            this.isInputDisabled = !this.isInputDisabled;
        }
    },
});