Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})
new Vue({
    el: '#app',
    data: {
        obj: {
            foo: 'bar'
        },
        categoryList: [
            { id: 0, text: 'Iphone' },
            { id: 1, text: 'SamSung' },
            { id: 2, text: 'OPPO' },
            { id: 3, text: 'HTC' }
        ]
    },
    methods: {

    },
    beforeCreate() {
        console.log('beforeCreate', this.categoryList, document.querySelector('#todo-list'));
    },
    created() {
        console.log('created', this.categoryList, document.querySelector('#todo-list'));
    },
    beforeUpdate() {
    },
    updated() {
    },
    beforeMount() {
        console.log('beforeMount', this.categoryList, document.querySelector('#todo-list'));
    },
    mounted() {
        console.log('mounted', this.categoryList, document.querySelector('#todo-list'));
    },
    beforeDestroy() {
        console.log('beforeDestroy', this.categoryList, document.querySelector('#todo-list'));
    },
    destroyed() {
        console.log('destroyed', this.categoryList, document.querySelector('#todo-list'));
    },
})