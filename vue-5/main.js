var app = new Vue({
    el: "#app",
    data: {
        a: 0,
        b: 0,
        number: 0,
        message: 'Hello World',
        firstName: 'Tiền',
        lastName: 'Nguyễn',
        question: '',
        answer: 'I cannot give you an answer until you ask a question!'
    },
    watch: {
        question: function(newQuestion, oldQuestion) {
            this.answer = 'Waiting for you to stop typing...'
        }
    },
    computed: {
        reversedMessage: function() {
            return this.message.split('').reverse().join('')
        },
        fullName: {
            get: function() {
                return this.firstName + ' ' + this.lastName
            },
            set: function(newValue) {
                var names = newValue.split(' ')
                this.firstName = names[0]
                this.lastName = names[names.length - 1]
            }
        }
    },
    methods: {
        addA() {
            console.log('add A');
            return this.a;
        },
        addB() {
            console.log('add B');
            return this.b;
        }
    },
});